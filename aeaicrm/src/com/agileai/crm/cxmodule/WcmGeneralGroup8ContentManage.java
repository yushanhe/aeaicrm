package com.agileai.crm.cxmodule;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface WcmGeneralGroup8ContentManage
        extends TreeAndContentManage {

	List<DataRow> findImportTempRecords(DataParam param);

	void createtImportTempRecords(List<DataParam> paramList);

	void deleteImportTempRecords(DataParam param);

	void batchDeleteTempRecords(List<DataParam> paramList);
}
