package com.agileai.crm.cxmodule;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface TaskCycle8ContentManage
        extends TreeAndContentManage {
	
	public void generatPlanRecord(DataParam param);
	
	public List<DataRow> findCustPlanVisitRecords(DataParam param);
	
	public List<DataRow> findProPlanVisitRecords(DataParam param);
	
	public List<DataRow> queryPeriodicTreeRecords(DataParam param);
	
	public List<DataRow> queryUnfinishedTaskRecords(DataParam param);
	
	public DataRow getSummaryRecord(DataParam param);
	
	public DataRow getProNumRecord(DataParam param);
	
	public DataRow getCustNumRecord(DataParam param);
	
	public void updateSummaryRecord(DataParam param);
	
	public void aggregateyRecord(String taskReviewId,String orderNum,String oppNum);

	public void updatePlanRecord(String taskReviewId,String taskReviewState);
	
	public void planTaskRecord(String taskReviewId);
	
	public List<DataRow> queryOppRecords(DataParam param);
	
	public List<DataRow> queryOrderRecords(DataParam param);
	
	public void deleteTaskReviewRecord(String columnId);
	
	public void updateTaskReviewDescRecord(DataParam param);
	
	public List<DataRow> findHomeCardRecords(DataParam param);
	
}
