package com.agileai.crm.module.procustomer.exteral;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/rest")
public interface ProCustomer {
	
	@POST  
    @Path("/list")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String findPotentialCustomerList(String searchWord);
    
    @GET  
    @Path("/search-list/{orgName}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findPotentialCustomerSearch(@PathParam("orgName") String orgName);
    
    @POST
    @Path("/add-cust-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String addPotentialCustomer(String info);
    
    @POST
    @Path("/update-cust-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String editPotentialCustomer(String info);
    
    @GET
    @Path("/get-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String getPotentialCustomerinfo(@PathParam("id") String id);
    
    @GET  
    @Path("/get-visit-info/{id}/{category}")
    @Produces(MediaType.TEXT_PLAIN)
	public String getPotentialVisitingRecord(@PathParam("id") String id, @PathParam("category") String category);
    
    @GET  
    @Path("/delete-visit-info/{id}")
    @Produces(MediaType.TEXT_PLAIN)
	public String deletePotentialCustomer(@PathParam("id") String id);
    
    @GET  
    @Path("/assign-cust-info/{id}/{saleId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String assignPotentialCustomer(@PathParam("id") String id, @PathParam("saleId") String saleId);
    
    @POST  
    @Path("/find-sale-list")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
	public String findSaleList(String saleName);
}
