package com.agileai.crm.module.system.service;

import java.util.List;

import com.agileai.crm.cxmodule.WcmGeneralGroup8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;

public class WcmGeneralGroup8ContentManageImpl
        extends TreeAndContentManageImpl
        implements WcmGeneralGroup8ContentManage {
    public WcmGeneralGroup8ContentManageImpl() {
        super();
        this.columnIdField = "GRP_ID";
        this.columnParentIdField = "GRP_PID";
        this.columnSortField = "GRP_ORDERNO";
    }
    @Override
	public List<DataRow> findImportTempRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findImportTempRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}
    @Override
	public void createtImportTempRecords(List<DataParam> paramList) {
		String statementId = sqlNameSpace+"."+"insertImportTempRecords";
		this.daoHelper.batchInsert(statementId, paramList);
	}
    @Override
	public void deleteImportTempRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteImportTempRecords";
		this.daoHelper.deleteRecords(statementId, param);	
	}
    @Override
	public void batchDeleteTempRecords(List<DataParam> paramList) {
		String statementId = sqlNameSpace+"."+"deleteImportTempRecords";
		this.daoHelper.batchInsert(statementId, paramList);
	}
}
