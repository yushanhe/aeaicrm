package com.agileai.crm.module.mytasks.handler;

import java.util.List;

import com.agileai.crm.cxmodule.MyCustomerSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class MyCustomerSelectListHandler
        extends PickFillModelHandler {
    public MyCustomerSelectListHandler() {
        super();
        this.serviceId = buildServiceId(MyCustomerSelect.class);
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		User user = (User) getUser();
		param.put("CUST_SALESMAN", user.getUserId());
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().queryPickFillRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }
    
    protected MyCustomerSelect getService() {
        return (MyCustomerSelect) this.lookupService(this.getServiceId());
    }
}
