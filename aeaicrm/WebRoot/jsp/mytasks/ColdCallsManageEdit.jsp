<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>陌生拜访</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveMasterRecord(){
	if (validate()){
		if (ele("currentSubTableId")){
			var subTableId = $("#currentSubTableId").val();
			if (!checkEntryRecords(subTableId)){
				return;
			}
		}
		showSplash();
		postRequest('form1',{actionType:'saveMasterRecord',onComplete:function(responseText){
			if ("fail" != responseText){
				$('#operaType').val('update');
				$('#TASK_ID').val(responseText);
				doSubmit({actionType:'prepareDisplay'});
			}else{
				hideSplash();
				writeErrorMsg('保存操作出错啦！');
			}
		}});
	}
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
}
function refreshPage(){
	doSubmit({actionType:'changeSubTable'});
}
function addEntryRecord(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'addEntryRecord'});
}
function deleteEntryRecord(subTableId){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要删除该条记录吗？',function(r){
		if(r){
			$('#currentSubTableId').val(subTableId);
			doSubmit({actionType:'deleteEntryRecord'});	
		}
	});
}
function checkEntryRecords(subTableId){
	var result = true;
	var currentRecordSize = $('#currentRecordSize').val();
	return result;
}
var viewSubRecordBox;
function viewSubRecordRequest(operaType,title,handlerId,subPKField){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!viewSubRecordBox){
		viewSubRecordBox = new PopupBox('viewSubRecordBox',title,{size:'normal',height:'350px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	viewSubRecordBox.sendRequest(url);
}
var operaPostponeTaskBox;
function openPostponeBox(){
	var handlerId = "PostponeManage";
	if (!operaPostponeTaskBox){
		operaPostponeTaskBox = new PopupBox('operaPostponeTaskBox','暂缓',{size:'normal',width:'600',top:'2px'});
	}
	var url = 'index?'+handlerId+'&ORG_ID='+$('#ORG_ID').val()+'&TASK_ID='+$('#TASK_ID').val()+'&TASK_REVIEW_ID='+$('#TASK_REVIEW_ID').val();
	operaPostponeTaskBox.sendRequest(url);
}
var operaFollowUpTaskBox;
function openFollowUpBox(){
	var handlerId = "FollowUpManage";
	if (!operaFollowUpTaskBox){
		operaFollowUpTaskBox = new PopupBox('operaFollowUpTaskBox','跟进',{size:'big',width:'850',height:'600',top:'2px'});
	}
	var url = 'index?'+handlerId+'&VISIT_CUST_ID='+$('#CUST_ID').val()+'&TASK_ID='+$('#TASK_ID').val()+'&ORG_ID='+$('#ORG_ID').val()+'&ORG_NAME='+$('#ORG_NAME').val()
			+'&TASK_REVIEW_ID='+$('#TASK_REVIEW_ID').val();
	operaFollowUpTaskBox.sendRequest(url);
}
function goBack(){
	doSubmit({actionType:'goBack'});
}
function goBackId(reviewId){
	$('#TASK_REVIEW_ID').val(reviewId);
	doSubmit({actionType:'goBack'});
}
function doNoFollowUp(){
	jConfirm('确认放弃此客户么？',function(r){
		if(r){
			$('#ORG_ID').val();
			$('#TASK_REVIEW_ID').val();
			doSubmit({actionType:'noFollowUp'});
		}
	});
}
var openProcustInfoEditBox;
function openProcustInfoBox(){
	var handlerId = "ProcustInfoEdit";
	if (!openProcustInfoEditBox){
		openProcustInfoEditBox = new PopupBox('openProcustInfoEditBox','潜在客户',{size:'big',width:'870',height:'600',top:'2px'});
	}
	var url = 'index?'+handlerId+'&ORG_ID='+$('#ORG_ID').val();
	openProcustInfoEditBox.sendRequest(url);
}
function doRefresh(){
	doSubmit({actionType:'prepareDisplay'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="padding-top:7px;">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
<div class="newarticle1" onclick="changeSubTable('_base')">基础信息</div>
<div class="newarticle1" onclick="changeSubTable('ProCustVisitInfo')">拜访记录</div>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;">
<div style="margin:2px;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if (!"ConfirmSummary".equals(pageBean.inputValue("TASK_REVIEW_STATE"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openPostponeBox()" ><input value="&nbsp;" type="button" class="editImgBtn" id="postponeImgBtn" title="暂缓" />暂缓</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openFollowUpBox()"><input value="&nbsp;" type="button" class="saveImgBtn" id="followUpImgBtn" title="跟进" />跟进</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openProcustInfoBox()"><input value="&nbsp;" type="button" class="editImgBtn" id="postponeImgBtn" title="编辑客户" />编辑客户</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doNoFollowUp()"><input value="&nbsp;" type="button" class="disposalImgBtn" id="disposalImgBtn" title="放弃" />放弃</td>
<%} %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>客户名称</th>
	<td><input id="ORG_NAME" label="客户名称" name="ORG_NAME" type="text" value="<%=pageBean.inputValue("ORG_NAME")%>" style="width: 302px" size="24" class="text" />
</td>
	<th width="100" nowrap>分类</th>
	<td colspan="3"><select id="ORG_CLASSIFICATION" label="分类" name="ORG_CLASSIFICATION" style="width: 303px" class="select"><%=pageBean.selectValue("ORG_CLASSIFICATION")%></select>
</td>
</tr>
<tr>
</tr>
<tr>
	<th width="100" nowrap>客户性质</th>
	<td><select id="ORG_TYPE" label="客户性质" name="ORG_TYPE" style="width: 303px" class="select"><%=pageBean.selectValue("ORG_TYPE")%></select>
</td>
	<th width="100" nowrap>来源渠道</th>
	<td colspan="3"><select id="ORG_SOURCES" label="来源渠道" name="ORG_SOURCES" style="width: 303px" class="select"><%=pageBean.selectValue("ORG_SOURCES")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>联系人</th>
	<td><input id="ORG_LINKMAN_NAME" label="联系人" name="ORG_LINKMAN_NAME" type="text" value="<%=pageBean.inputValue("ORG_LINKMAN_NAME")%>" style="width: 302px" size="24" class="text" />
</td>
	<th width="100" nowrap>邮箱</th>
	<td colspan="3"><input id="ORG_EMAIL" label="邮箱" name="ORG_EMAIL" type="text" value="<%=pageBean.inputValue("ORG_EMAIL")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>标签</th>
	<td><input id="ORG_LABELS_NAME" name="ORG_LABELS_NAME" type="text" value="<%=pageBean.inputValue("ORG_LABELS_NAME")%>" size="24" style="width: 302px" class="text" readonly="readonly" />
    <input id="ORG_LABELS" name="ORG_LABELS" type="hidden" value="<%=pageBean.inputValue("ORG_LABELS")%>" size="24" style="width: 302px" class="text"/>
</td>
	<th width="100" nowrap>公司网站</th>
	<td colspan="3"><input id="ORG_WEBSITE" label="公司网站" name="ORG_WEBSITE" type="text" value="<%=pageBean.inputValue("ORG_WEBSITE")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>跟进人员</th>
	<td><input name="ORG_SALESMAN_NAME" type="text" class="text"	id="ORG_SALESMAN_NAME"	value="<%=pageBean.inputValue("ORG_SALESMAN_NAME")%>" style="width: 302px" size="24"	readonly="readonly" label="跟进人员" /> 
		<input id="ORG_SALESMAN" label="跟进人员"  name="ORG_SALESMAN" type="hidden"	value="<%=pageBean.inputValue("ORG_SALESMAN")%>"  size="24" class="text" />
</td>
	<th width="100" nowrap>状态</th>
	<td colspan="3"><%=pageBean.selectRadio("ORG_STATE")%>
   </td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="ORG_CREATER_NAME" type="text" class="text"	id="ORG_CREATER_NAME"	value="<%=pageBean.inputValue("ORG_CREATER_NAME")%>" style="width: 302px" size="24"	readonly="readonly" label="创建人" /> 
		<input id="ORG_CREATER" label="创建人"  name="ORG_CREATER" type="hidden"	value="<%=pageBean.inputValue("ORG_CREATER")%>"  size="24" class="text" />
</td>
	<th width="100" nowrap>创建时间</th>
	<td colspan="3"><input id="ORG_CREATE_TIME" label="创建时间" name="ORG_CREATE_TIME" type="text" value="<%=pageBean.inputTime("ORG_CREATE_TIME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />	
</td>
</tr>
<tr>
	<th width="100" nowrap>更新时间</th>
	<td><input id="ORG_UPDATE_TIME" label="更新时间" name="ORG_UPDATE_TIME" type="text" value="<%=pageBean.inputTime("ORG_UPDATE_TIME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />
</td>
	<th width="100" nowrap>计划再次拜访时间</th>
	<td colspan="3"><input id="ORG_VISIT_AGAIN_TIME" label="更新时间" name="ORG_VISIT_AGAIN_TIME" type="text" value="<%=pageBean.inputTime("ORG_VISIT_AGAIN_TIME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>联系方式</th>
	<td><input id="ORG_CONTACT_WAY" label="联系方式" name="ORG_CONTACT_WAY" type="text" value="<%=pageBean.inputValue("ORG_CONTACT_WAY")%>" size="24" style="width: 302px" class="text" />
</td>
	<th width="100" nowrap>地址</th>
	<td colspan="3"><input id="ORG_ADDRESS" label="地址" name="ORG_ADDRESS" type="text" value="<%=pageBean.inputValue("ORG_ADDRESS")%>" size="24" style="width: 302px" class="text" />
</td>
</tr>
<tr>
</tr>
<tr>
<th width="100" nowrap>简要介绍</th>
	<td colspan="3"><textarea id="ORG_INTRODUCTION" label="简要介绍" name="ORG_INTRODUCTION" style="width:95%" cols="60" rows="5" maxlength="256" class="textarea"><%=pageBean.inputValue("ORG_INTRODUCTION")%></textarea>
</td>
</tr>
</table>
</div>
</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<%if ("ProCustVisitInfo".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer2" style="height:auto;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="viewSubRecordRequest('update','拜访记录','ProCustVisitInfo','PROCUST_VISIT_ID')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>      
</tr>   
   </table>
</div>
<div style="margin:2px;">
<%
List param1Records = (List)pageBean.getAttribute("ProCustVisitInfoRecords");
pageBean.setRsList(param1Records);
%>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="CRM_ORG_INFO.csv"
retrieveRowsCallback="process" xlsFileName="CRM_ORG_INFO.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="auto"
>
<ec:row styleClass="odd" ondblclick="viewSubRecordRequest('detail','拜访记录','ProCustVisitInfo','PROCUST_VISIT_ID')" onclick="selectRow(this,{PROCUST_VISIT_ID:'${row.PROCUST_VISIT_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="PROCUST_VISIT_DATE" title="拜访日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="PROCUST_VISIT_FILL_NAME" title="填写人" />
	<ec:column width="100" property="PROCUST_VISIT_FILL_TIME" title="填写时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="PROCUST_VISIT_EFFECT" title="拜访效果" mappingItem="PROCUST_VISIT_EFFECT"/>
	<ec:column width="50" property="PROCUST_VISIT_TYPE" title="拜访类型" mappingItem="PROCUST_VISIT_TYPE"/>
</ec:row>
</ec:table>
</div>
</div>
<script language="javascript">
setRsIdTag('PROCUST_VISIT_ID');
</script>
<%}%>


<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
<script language="javascript">
$("#Layer0").hide();
</script>
<%}%>
<%}%>
<script language="javascript">
new Tab('tab','tabHeader','Layer',<%=currentSubTableIndex%>);
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
$("#Layer0").hide();
<%}%>
</script>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="TASK_ID" name="TASK_ID" value="<%=pageBean.inputValue4DetailOrUpdate("TASK_ID","")%>" />
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>" />
<input type="hidden" name="PROCUST_VISIT_ID" id="PROCUST_VISIT_ID" value="<%=pageBean.inputValue("PROCUST_VISIT_ID")%>"/>
<input type="hidden" id="CUST_ID" name="CUST_ID" value="<%=pageBean.inputValue("CUST_ID")%>" />
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>" />
</div>
</form>
<script language="javascript">
initDetailOpertionImage();
$(function(){
	resetTabHeight(80);
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
