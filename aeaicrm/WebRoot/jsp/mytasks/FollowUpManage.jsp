<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function doSure(){
	if (!validate()){
		return;
	}
	postRequest('form1',{actionType:'save',onComplete:function(rspText){
		if(rspText=='success'){
			parent.goBackId($('#TASK_REVIEW_ID').val());
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSure()"><input value="&nbsp;" type="button" class="saveImgBtn" id="followUpImgBtn" title="确定" />确定</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="B" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>       
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>拜访类型</th>
	<td width="300"><select id="VISIT_TYPE" label="拜访类型" name="VISIT_TYPE" class="select">
	  <%=pageBean.selectValue("VISIT_TYPE")%>
	  </select></td>
	<th width="100" nowrap>接待人姓名</th>
	<td>
	<input id="VISIT_RECEPTION_NAME" label="联系人姓名" name="VISIT_RECEPTION_NAME" type="text" value="<%=pageBean.inputValue("VISIT_RECEPTION_NAME")%>" size="24" class="text" />
	<input id="CONT_ID" name="CONT_ID" type="hidden" value="<%=pageBean.inputValue("CONT_ID")%>"/>
</td>
</tr>
<tr>
<tr>
	<th width="100" nowrap>拜访人员</th>
	<td>
	  <input name="VISIT_USER_NAME" type="text" class="text" id="VISIT_USER_NAME" value="<%=pageBean.inputValue("VISIT_USER_NAME")%>" size="24" label="拜访人员" />
	  <input id="VISIT_USER_ID" label="拜访人员" name="VISIT_USER_ID" type="hidden" value="<%=pageBean.inputValue("VISIT_USER_ID")%>" size="24" class="text" hidden="hidden" />
      </td>
	<th width="100" nowrap>同行人员</th>
	<td><input id="VISIT_PEER_NAME" label="同行人员" name="VISIT_PEER_NAME" type="text" value="<%=pageBean.inputValue("VISIT_PEER_NAME")%>" size="24" class="text" /></td>
</tr>
<tr>
    <th width="100" nowrap>拜访时间</th>
	<td><input name="VISIT_DATE" type="text" class="text" id="VISIT_DATE" value="<%=pageBean.inputTime("VISIT_DATE")%>" size="24" readonly="readonly" label="拜访日期" />
	  <img id="VISIT_DATEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" /></td>
	<th width="100" nowrap>拜访费用</th>
	<td><input id="VISIT_COST" label="拜访费用" name="VISIT_COST" type="text" value="<%=pageBean.inputValue("VISIT_COST")%>" size="24" maxlength="6" class="text" /></td>    
</tr>
<tr>
	<th width="100" nowrap>沟通效果</th>
	<td><select id="VISIT_EFFECT" label="沟通效果" name="VISIT_EFFECT" style="width:213px" class="select"><%=pageBean.selectValue("VISIT_EFFECT")%></select></td>
	<th width="100" nowrap>状态</th>
	<td><input id="VISIT_STATE_TEXT" label="状态" name="VISIT_STATE_TEXT" type="text" value="<%=pageBean.selectedText("VISIT_STATE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="VISIT_STATE" label="状态" name="VISIT_STATE" type="hidden" value="<%=pageBean.selectedValue("VISIT_STATE")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>费用说明</th>
	<td colspan="3">
	  <textarea id="VISIT_COST_EXPLAIN" label="费用说明" name="VISIT_COST_EXPLAIN" cols="98" rows="4" class="textarea"><%=pageBean.inputValue("VISIT_COST_EXPLAIN")%></textarea>
	  </td>
</tr>
<tr>
	<th width="100" nowrap>交互内容</th>
	<td colspan="3"><textarea id="VISIT_CONTENT" label="交互内容" name="VISIT_CONTENT" cols="98" rows="4" class="textarea"><%=pageBean.inputValue("VISIT_CONTENT")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>客户关注点</th>
	<td colspan="3"><textarea id="VISIT_CUST_FOCUS" label="客户关注点" name="VISIT_CUST_FOCUS" cols="98" rows="4" class="textarea"><%=pageBean.inputValue("VISIT_CUST_FOCUS")%></textarea>
</td>
</tr>

<tr>
	<th width="100" nowrap>待改进情况</th>
	<td colspan="3">
	  <textarea name="VISIT_IMPROVEMENT" cols="98" rows="4" class="text" id="VISIT_IMPROVEMENT" label="待改进情况"><%=pageBean.inputValue("VISIT_IMPROVEMENT")%></textarea></td>
</tr>
<tr>
	<th width="100" nowrap>填写人</th>
	<td><input name="VISIT_FILL_NAME" type="text" class="text" id="VISIT_FILL_NAME" value="<%=pageBean.inputValue("VISIT_FILL_NAME")%>" size="24" readonly="readonly" label="填写人" />
    <input id="VISIT_FILL_ID" label="填写人" name="VISIT_FILL_ID" type="hidden" value="<%=pageBean.inputValue("VISIT_FILL_ID")%>" size="24" class="text" hidden="hidden" />
</td>
	<th width="100" nowrap>填写时间</th>
	<td><input id="VISIT_FILL_TIME" label="填写时间" name="VISIT_FILL_TIME" type="text" value="<%=pageBean.inputTime("VISIT_FILL_TIME")%>" size="24" class="text" readonly="readonly"/></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>" />
<input type="hidden" id="ORG_ID" name="ORG_NAME" value="<%=pageBean.inputValue("ORG_NAME")%>" />
<input type="hidden" name="VISIT_CUST_ID" id="VISIT_CUST_ID" value="<%=pageBean.inputValue("VISIT_CUST_ID")%>" />
<input type="hidden" id="TASK_ID" name="TASK_ID" value="<%=pageBean.inputValue("TASK_ID")%>" />
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>" />
</form>
<script language="javascript">
$('#VISIT_COST_EXPLAIN').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
$('#VISIT_CONTENT').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
$('#VISIT_CUST_FOCUS').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
$('#VISIT_IMPROVEMENT').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
initCalendar('VISIT_DATE','%Y-%m-%d %H:%m','VISIT_DATEPicker');
datetimeValidators[0].set("yyyy-MM-dd HH:mm").add("VISIT_DATE");
numValidator.add("VISIT_COST");
datetimeValidators[1].set("yyyy-MM-dd HH:mm").add("VISIT_FILL_TIME");
datetimeValidators[2].set("yyyy-MM-dd HH:mm").add("VISIT_CONFIRM_TIME");
initDetailOpertionImage();
requiredValidator.add("VISIT_DATE");
requiredValidator.add("VISIT_EFFECT");
requiredValidator.add("VISIT_TYPE");
requiredValidator.add("VISIT_RECEPTION_NAME");
requiredValidator.add("VISIT_CONTENT");
requiredValidator.add("VISIT_CUST_FOCUS");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
